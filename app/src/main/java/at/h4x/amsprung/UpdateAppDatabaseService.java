// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;

import com.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import at.h4x.amsprung.room.AppDatabase;
import at.h4x.amsprung.room.model.Line;
import at.h4x.amsprung.room.model.LineRoute;
import at.h4x.amsprung.room.model.LineRouteDao;
import at.h4x.amsprung.room.model.LineType;
import at.h4x.amsprung.room.model.LineDao;
import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.room.model.StationDao;
import at.h4x.amsprung.room.model.Stop;
import at.h4x.amsprung.room.model.StopDao;
import at.h4x.amsprung.util.MinLog;

/**
 * Docs: https://developer.android.com/guide/components/services
 */
public class UpdateAppDatabaseService extends Service {

    private static final String ACTION_UPDATE_DATABASE = "at.h4x.amsprung.action.UPDATE_DATABASE";
    private static final int NOTIFICATION_ID = 44;

    public static final char SEPARATOR = ';';
    public static final char QUOTE_CHAR = '"';

    private HandlerThread serviceThread;
    private Handler serviceHandler;

    public UpdateAppDatabaseService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        serviceThread = new HandlerThread(
                UpdateAppDatabaseService.class.getName(),
                HandlerThread.MIN_PRIORITY);
        serviceThread.start();
        serviceHandler = new Handler(serviceThread.getLooper());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(Build.VERSION.SDK_INT >= 18) {
            serviceThread.quitSafely();
        } else {
            serviceThread.quit();
        }
    }

    @Override public IBinder onBind(Intent intent) {return null;}

    public static void startActionUpdateDatabase(Context context) {
        Intent intent = new Intent(context, UpdateAppDatabaseService.class);
        intent.setAction(ACTION_UPDATE_DATABASE);
        context.startService(intent);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (ACTION_UPDATE_DATABASE.equals(intent.getAction())){
            handleActionUpdateDatabase();
        }

        return START_NOT_STICKY;
    }

    private void handleActionUpdateDatabase() {

        serviceHandler.post(new Runnable() {
            @Override
            public void run() {

                PendingIntent pi = PendingIntent.getActivity(UpdateAppDatabaseService.this, 0,
                        new Intent(UpdateAppDatabaseService.this, HomeActivity.class), PendingIntent.FLAG_IMMUTABLE);

                Notification n =
                        new NotificationCompat.Builder(UpdateAppDatabaseService.this, NotificationHelper.FOREGROUND_SERVICE_CHANNEL_ID)
                                .setSmallIcon(R.mipmap.ic_launcher_foreground)
                                .setContentText(getString(R.string.update_database_notification))
                                .setContentIntent(pi)
                                .build();

                startForeground(NOTIFICATION_ID, n);

                // get ready for accessing database
                AppDatabase db = AppDatabase.via(UpdateAppDatabaseService.this);
                importStops(db.stopDao());
                importLines(db.lineDao());
                importLineRoutes(db.lineRouteDao());

                importStations(db.stationDao(), db.lineRouteDao());

                stopForeground(true);
            }
        });

    }

    private void importStops(StopDao stopDao) {
        int insertCount = 0;
        int updateCount = 0;
        try {
            String[] line = null;
            CSVReader r = new CSVReader(new InputStreamReader(this.getResources().openRawResource(R.raw.wienerlinien_ogd_haltepunkte)), SEPARATOR, QUOTE_CHAR);

            // parse header
            Map<String, Integer> header = new HashMap<String, Integer>();
            line = r.readNext();
            for(int i=0; i<line.length; i++) {
                header.put(line[i].toLowerCase(Locale.ROOT), i);
            }

            while ((line = r.readNext()) != null) {
                try {
                    Stop stop = new Stop();
                    stop.setId(intOrNull(line[header.get("stopid")]));
                    stop.setStationId(intOrNull(line[header.get("diva")]));
                    stop.setName(line[header.get("stoptext")]);
                    stop.setCommuneName(line[header.get("municipality")]);
                    stop.setCommuneId(intOrNull(line[header.get("municipalityid")]));
                    stop.setLat(doubleOrNull(line[header.get("latitude")]));
                    stop.setLng(doubleOrNull(line[header.get("longitude")]));

                    if (stopDao.byId(stop.getId()) == null) {
                        stopDao.insert(stop);
                        insertCount++;
                    } else {
                        stopDao.update(stop);
                        updateCount++;
                    }
                } catch (NumberFormatException e) {
                    MinLog.d(e, "failed to import line \"%s\"", join(", ", line));
                } catch (NullPointerException e) {
                    MinLog.d(e, "failed to import line \"%s\"", join(", ", line));
                }
            }
            MinLog.i("done importing stops, (%d updated, %d inserted)", updateCount, insertCount);
        } catch (FileNotFoundException e) {
            MinLog.d(e, "could not open stop-definitions");
        } catch (IOException e) {
            MinLog.d(e, "could not read stop-definitions");
        }
    }

    private void importStations(StationDao stationDao, LineRouteDao lineDao) {
        int updateCount = 0;
        int insertCount = 0;
        int blockCount = 0;
        try {
            String[] line = null;
            CSVReader r = new CSVReader(new InputStreamReader(this.getResources().openRawResource(R.raw.wienerlinien_ogd_haltestellen)), SEPARATOR, QUOTE_CHAR);

            // parse header
            Map<String, Integer> header = new HashMap<String, Integer>();
            line = r.readNext();
            for(int i=0; i<line.length; i++) {
                header.put(line[i].toLowerCase(Locale.ROOT), i);
            }

            while ((line = r.readNext()) != null) {
                Station station = new Station();
                station.setId(Integer.valueOf(line[header.get("diva")]));
                if (lineDao.stationHasLines(station.getId())) {
                    station.setName(line[header.get("platformtext")]);
                    station.setCommuneName(line[header.get("municipality")]);
                    station.setLatitude(doubleOrNull(line[header.get("latitude")]));
                    station.setLongitude(doubleOrNull(line[header.get("longitude")]));

                    if (stationDao.byId(station.getId()) != null) {
                        stationDao.update(station);
                        updateCount++;
                    } else {
                        stationDao.insert(station);
                        insertCount++;
                    }
                } else {
                    blockCount++;
                    stationDao.delete(station);
                }
            }
            MinLog.i("done importing lines, (%d updated, %d inserted, %d blocked)", updateCount, insertCount, blockCount);

        } catch (IOException e) {
            MinLog.d(e, "could not read line-definitions");
        }
    }

    private void importLines(LineDao lineDao) {
        int updateCount = 0;
        int insertCount = 0;
        try {
            String[] line = null;
            CSVReader r = new CSVReader(new InputStreamReader(this.getResources().openRawResource(R.raw.wienerlinien_ogd_linien)), SEPARATOR, QUOTE_CHAR);

            // parse header
            Map<String, Integer> header = new HashMap<String, Integer>();
            line = r.readNext();
            for(int i=0; i<line.length; i++) {
                header.put(line[i].toLowerCase(Locale.ROOT), i);
            }

            while ((line = r.readNext()) != null) {
                Line l = new Line();
                l.setId(Integer.valueOf(line[header.get("lineid")]));
                l.setName(line[header.get("linetext")]);
                l.setRealTimeData("1".equals(line[header.get("realtime")]));
                l.setType(LineType.fromName(line[header.get("meansoftransport")]));

                if (lineDao.byId(l.getId()) != null) {
                    lineDao.update(l);
                    updateCount++;
                } else {
                    lineDao.insert(l);
                    insertCount++;
                }
            }
            MinLog.i("done importing lines, (%d updated, %d inserted)", updateCount, insertCount);

        } catch (IOException e) {
            MinLog.d(e, "could not read line-definitions");
        }
    }

    private void importLineRoutes(LineRouteDao lineRouteDao) {
        int updateCount = 0;
        int insertCount = 0;
        try {
            String[] line = null;
            CSVReader r = new CSVReader(new InputStreamReader(this.getResources().openRawResource(R.raw.wienerlinien_ogd_fahrwegverlaeufe)), SEPARATOR, QUOTE_CHAR);

            // parse header
            Map<String, Integer> header = new HashMap<String, Integer>();
            line = r.readNext();
            for(int i=0; i<line.length; i++) {
                header.put(line[i].toLowerCase(Locale.ROOT), i);
            }

            while ((line = r.readNext()) != null) {
                LineRoute lineRoute = new LineRoute();
                lineRoute.setLineId(Integer.valueOf(line[header.get("lineid")]));
                lineRoute.setStopId(Integer.valueOf(line[header.get("stopid")]));
                lineRoute.setPatternId(Integer.valueOf(line[header.get("patternid")]));
                lineRoute.setStopSequenceCount(Integer.valueOf(line[header.get("stopseqcount")]));

                if (lineRouteDao.byIds(lineRoute.getLineId(), lineRoute.getStopId()) != null) {
                    lineRouteDao.update(lineRoute);
                    updateCount++;
                } else {
                    lineRouteDao.insert(lineRoute);
                    insertCount++;
                }
            }
            MinLog.i("done importing lines, (%d updated, %d inserted)", updateCount, insertCount);

        } catch (IOException e) {
            MinLog.d(e, "could not read line-definitions");
        }
    }

    static Integer intOrNull(String value) {
        if (value == null || "".equals(value)) {
            return null;
        }
        return Integer.valueOf(value);
    }

    static Double doubleOrNull(String value) {
        if (value == null || "".equals(value)) {
            return null;
        }
        return Double.parseDouble(value);
    }

    static String join(String delimiter, String[] values) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String value : values) {
            if (first) {
                first = false;
            } else {
                sb.append(delimiter);
            }
            sb.append(value);
        }
        return sb.toString();
    }
}
