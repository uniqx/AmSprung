// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.wienerlinienapi.LineBadgeHelper;

class StationsAdapter extends RecyclerView.Adapter<StationsAdapter.ViewHolder> {

    private final Handler mainHandler = new Handler(Looper.getMainLooper());
    private volatile List<Station> stations;
    private final ClickListener listener;

    public StationsAdapter(ClickListener listener) {
        stations = Arrays.asList();
        this.listener = listener;
    }

    public void updateStationsAdapter(List<Station> stations) {
        synchronized (StationsAdapter.this) {
            this.stations = Collections.unmodifiableList(stations);
        }
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_station, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        synchronized (StationsAdapter.this) {
            if (position < stations.size()) {
                Station station = stations.get(position);

                holder.stationName.setText(station.getName());
                holder.communeName.setText(station.getCommuneName());
                LineBadgeHelper.lineBadgesForStation(station.getId(), holder.lines);

                final int stationId = station.getId();
                holder.clicky.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.onClicked(stationId);
                        }
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        synchronized (StationsAdapter.this) {
            return stations.size();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View clicky;
        TextView stationName;
        TextView communeName;
        TextView distance;
        LinearLayout lines;

        public ViewHolder(View itemView) {
            super(itemView);
            clicky = itemView.findViewById(R.id.item);
            stationName = (TextView) itemView.findViewById(R.id.stationName);
            communeName = (TextView) itemView.findViewById(R.id.stationCommune);
            distance = (TextView) itemView.findViewById(R.id.distance);
            lines = (LinearLayout) itemView.findViewById(R.id.lines);
        }
    }

    interface ClickListener {
        void onClicked(int stationId);
    }
}
