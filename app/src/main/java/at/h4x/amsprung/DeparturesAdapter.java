// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import at.h4x.amsprung.room.model.LineType;
import at.h4x.amsprung.wienerlinienapi.LineBadgeHelper;
import at.h4x.amsprung.wienerlinienapi.MonitorInfoAsyncTask;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by uniq on 21.01.17.
 */
public class DeparturesAdapter extends RecyclerView.Adapter<DeparturesAdapter.DeparturesViewHolder>{

    List<Dep> deps;

    public DeparturesAdapter(List<MonitorInfoAsyncTask.MonitorInfo> monitorInfos) {

        deps = new ArrayList<>();

        for (MonitorInfoAsyncTask.MonitorInfo info : monitorInfos) {
            if(info.getData() != null && info.getData().getMonitors() != null) {
                for (MonitorInfoAsyncTask.Monitor monitor : info.getData().getMonitors()) {
                    if (monitor.getLines() != null) {
                        for (MonitorInfoAsyncTask.Line line : monitor.getLines()) {
                            if (line.getDepartures() != null && line.getDepartures().getDeparture() != null) {
                                for (MonitorInfoAsyncTask.Departure departures : line.getDepartures().getDeparture()) {
                                    Dep dep = new Dep();
                                    dep.setCountdown(departures.getDepartureTime().getCountdown());
                                    dep.setLineName(line.getName());
                                    dep.setTowards(line.getTowards());
                                    dep.setLineType(LineType.fromName(line.getType()));
                                    if(!deps.contains(dep)) {
                                        // apis might be sending duplicates...
                                        deps.add(dep);
                                    }
                                    // MinLog.v("parsed: %s %s %s %d", dep.getLineName(), dep.getTowards(), dep.getLineType(), dep.getCountdown());
                                }
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(deps, new Comparator<Dep>() {
            @Override
            public int compare(Dep o1, Dep o2) {
                return o1.getCountdown() - o2.getCountdown();
            }
        });

        // for (Dep dep : deps){
        //     MinLog.v("dep: %s %s %s %d", dep.getLineName(), dep.getTowards(), dep.getLineType(), dep.getCountdown());
        // }
    }

    @Override
    public DeparturesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_departure, parent, false);
        // set the view's size, margins, paddings and layout parameters
        DeparturesViewHolder vh = new DeparturesViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DeparturesViewHolder holder, int position) {
        Dep dep = deps.get(position);
        holder.line.setText(dep.getLineName());
        LineBadgeHelper.format(holder.line, dep.getLineName(), dep.getLineType());
        holder.toward.setText(dep.getTowards());

        if (dep.getCountdown() == 0) {
            holder.countdown.setText("*");
        } else {
            holder.countdown.setText(dep.getCountdown() + "\"");
        }
    }

    @Override
    public int getItemCount() {
        return deps.size();
    }

    public class DeparturesViewHolder extends RecyclerView.ViewHolder{

        View item;
        TextView line;
        TextView toward;
        TextView countdown;

        public DeparturesViewHolder(View itemView) {
            super(itemView);
            item = itemView.findViewById(R.id.item);
            toward = (TextView) itemView.findViewById(R.id.toward);
            countdown = (TextView) itemView.findViewById(R.id.countdown);
            line = (TextView) itemView.findViewById(R.id.line);
        }
    }

    @EqualsAndHashCode
    public class Dep{
        @Getter @Setter String lineName;
        @Getter @Setter LineType lineType;
        @Getter @Setter String towards;
        @Getter @Setter int countdown;
    }
}
