// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import lombok.Getter;
import lombok.Setter;

/**
 * imported from: wienerlinien_ogd_fahrwegverlaeufe.csv
 * * id ... artificial
 * * lineId = LineID
 * * stopId = StopID
 */
@Entity(tableName = LineRoute.TABLE_NAME)
public class LineRoute {

    public static final String TABLE_NAME = "line_route";
    public static final String C_LINE_ID = "line_id";
    public static final String C_PATTERN_ID = "pattern_id";
    public static final String C_STOP_SEQUENCE_COUNT = "stop_sequence_count";
    public static final String C_STOP_ID = "stop_id";

    @PrimaryKey(autoGenerate = true)
    @Getter @Setter int id;

    @ColumnInfo(name = C_LINE_ID, index = true)
    @Getter @Setter int lineId;

    @ColumnInfo(name = C_PATTERN_ID)
    @Getter @Setter int patternId;

    @ColumnInfo(name = C_STOP_SEQUENCE_COUNT)
    @Getter @Setter int stopSequenceCount;

    @ColumnInfo(name = C_STOP_ID, index = true)
    @Getter @Setter int stopId;
}