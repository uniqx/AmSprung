// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Entity(tableName = FavouriteStation.TABLE_NAME)
public class FavouriteStation {

    public static final String TABLE_NAME = "fav_station";
    public static final String C_ID = "id";

    @PrimaryKey
    @Getter @Setter int id;
}
