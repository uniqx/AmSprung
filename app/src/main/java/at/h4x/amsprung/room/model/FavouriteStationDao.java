// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import at.h4x.amsprung.room.model.FavouriteStation;
import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.room.model.Stop;

@Dao
public interface FavouriteStationDao {
    @Query("select * from " + FavouriteStation.TABLE_NAME)
    List<FavouriteStation> getAll();

    @Query("select * from " + FavouriteStation.TABLE_NAME + " where " + FavouriteStation.C_ID + " = :id")
    FavouriteStation byId(int id);

    @Query("select * from " + FavouriteStation.TABLE_NAME + " where " + FavouriteStation.C_ID + " in (:ids)")
    List<FavouriteStation> byIds(int... ids);

    @Insert
    void insert(FavouriteStation... favouriteStations);

    @Update
    void update(FavouriteStation... favouriteStations);

    @Delete
    void delete(FavouriteStation favouriteStation);

    @Query("delete from " + FavouriteStation.TABLE_NAME + " where " + FavouriteStation.C_ID + " = :stopId")
    void deleteById(int stopId);

    @Query("select * from " + Station.TABLE_NAME + " where " + Stop.C_ID + " in (select * from " + FavouriteStation.TABLE_NAME + ")")
    List<Station> loadAllFavouritedStops();
}
