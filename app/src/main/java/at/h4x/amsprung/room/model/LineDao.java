// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LineDao {
    @Query("select * from " + Line.TABLE_NAME)
    List<Line> getAll();

    @Query("select * from " + Line.TABLE_NAME + " where " + Line.C_ID + " = :id")
    Line byId(int id);

    @Query("select * from " + Line.TABLE_NAME + " where " + Line.C_ID + " in (:ids)")
    List<Line> byIds(int... ids);

    @Insert
    void insert(Line... lines);

    @Update
    void update(Line... lines);

    @Delete
    void delete(Line line);

    @Query("select * from " + Line.TABLE_NAME + " where " + Line.C_ID + " in (select " + LineRoute.C_LINE_ID + " from " + LineRoute.TABLE_NAME + " where " + LineRoute.C_STOP_ID + " in (select " + Stop.C_ID + " from " + Stop.TABLE_NAME + " where " + Stop.C_STATION_ID + " = :stationId))")
    List<Line> getLinesForStationId(int stationId);
}
