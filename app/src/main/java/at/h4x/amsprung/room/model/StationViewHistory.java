// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

/**
 *
 */
@Entity(tableName = StationViewHistory.TABLE_NAME)
public class StationViewHistory {

    public static final String TABLE_NAME = "stop_view_hist";
    public static final String C_ID = "id";
    public static final String C_LAST_VIEWED = "last_viewed";
    public static final String C_VIEW_COUNT = "view_count";

    @PrimaryKey
    @Getter @Setter int id;

    @ColumnInfo(name = C_LAST_VIEWED)
    @Getter @Setter long lastViewed;

    @ColumnInfo(name = C_VIEW_COUNT)
    @Getter @Setter int viewCount;
}
