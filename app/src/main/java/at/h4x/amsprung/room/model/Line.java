// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import lombok.Getter;
import lombok.Setter;

/**
 * imported from: wienerlinien_ogd_linien.csv
 * id = LineID
 */
@Entity(tableName = Line.TABLE_NAME)
public class Line {

    public static final String TABLE_NAME = "line";
    public static final String C_ID = "id";
    public static final String C_NAME = "name";
    public static final String C_REAL_TIME_DATA = "rtdata";
    public static final String C_LINE_TYPE = "line_type";

    @PrimaryKey
    @Getter @Setter int id;

    @ColumnInfo(name = C_NAME)
    @Getter @Setter String name;

    @ColumnInfo(name = C_REAL_TIME_DATA)
    @Getter @Setter boolean realTimeData;

    @ColumnInfo(name = C_LINE_TYPE)
    @TypeConverters(LineType.Converter.class)
    @Getter @Setter
    LineType type;
}
