// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import at.h4x.amsprung.room.model.Stop;

@Dao
public interface StopDao {
    @Query("select * from " + Stop.TABLE_NAME)
    List<Stop> getAll();

    @Query("select * from " + Stop.TABLE_NAME + " where " + Stop.C_ID + " = :id")
    Stop byId(int id);

    @Query("select * from " + Stop.TABLE_NAME + " where " + Stop.C_ID + " = :ids")
    List<Stop> byIds(int... ids);

    @Query("select * from " + Stop.TABLE_NAME + " where " + Stop.C_NAME + " like :name")
    List<Stop> findByName(String name);

    @Insert
    void insert(Stop... stops);

    @Update
    void update(Stop... stops);

    @Delete
    void delete(Stop stop);

}
