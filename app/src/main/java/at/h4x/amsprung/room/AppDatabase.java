// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room;


import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import android.content.Context;

import at.h4x.amsprung.room.model.FavouriteStation;
import at.h4x.amsprung.room.model.FavouriteStationDao;
import at.h4x.amsprung.room.model.Line;
import at.h4x.amsprung.room.model.LineDao;
import at.h4x.amsprung.room.model.LineRoute;
import at.h4x.amsprung.room.model.LineRouteDao;
import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.room.model.StationDao;
import at.h4x.amsprung.room.model.StationViewHistory;
import at.h4x.amsprung.room.model.StationViewHistoryDao;
import at.h4x.amsprung.room.model.Stop;
import at.h4x.amsprung.room.model.StopDao;


/**
 * Docs: https://developer.android.com/topic/libraries/architecture/room
 */
// TODO: re-evaluate setting exportSchema to false (just false to silence lint atm.)
@Database(entities = {Stop.class, Station.class, Line.class, LineRoute.class, FavouriteStation.class, StationViewHistory.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase{

    private static volatile AppDatabase INSTANCE;

    public abstract StopDao stopDao();
    public abstract StationDao stationDao();
    public abstract LineDao lineDao();
    public abstract LineRouteDao lineRouteDao();
    public abstract FavouriteStationDao favouriteStationDao();
    public abstract StationViewHistoryDao stationViewHistoryDao();

    public static AppDatabase via(Context context) {

        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "appDatabase.db")
                            .fallbackToDestructiveMigrationFrom(1)
                            .fallbackToDestructiveMigrationFrom(2)
                            .build();
                }
            }
        }
        return INSTANCE;
    }



}
