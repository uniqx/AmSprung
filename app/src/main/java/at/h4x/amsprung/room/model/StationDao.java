// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import at.h4x.amsprung.room.model.Station;

@Dao
public interface StationDao {
    @Query("select * from " + Station.TABLE_NAME)
    List<Station> getAll();

    @Query("select * from " + Station.TABLE_NAME + " where " + Station.C_ID + " = :id")
    Station byId(int id);

    @Query("select * from " + Station.TABLE_NAME + " where " + Station.C_ID + " = :ids")
    List<Station> byIds(int... ids);

    @Query("select * from " + Station.TABLE_NAME + " where " + Station.C_NAME + " like :name")
    List<Station> findByName(String name);

    @Insert
    void insert(Station... stops);

    @Update
    void update(Station... stops);

    @Delete
    void delete(Station stop);

}
