// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.room.model.StationViewHistory;

@Dao
public interface StationViewHistoryDao {
    @Query("select * from " + StationViewHistory.TABLE_NAME)
    List<StationViewHistory> getAll();

    @Query("select * from " + StationViewHistory.TABLE_NAME + " where " + StationViewHistory.C_ID + " = :id")
    StationViewHistory byId(int id);

    @Query("select * from " + StationViewHistory.TABLE_NAME + " where " + StationViewHistory.C_ID + " in (:ids)")
    List<StationViewHistory> byIds(int... ids);

    @Insert
    void insert(StationViewHistory... favouriteStations);

    @Update
    void update(StationViewHistory... favouriteStations);

    @Delete
    void delete(StationViewHistory favouriteStations);

    @Query("select " + Station.TABLE_NAME + ".* from " + Station.TABLE_NAME + " inner join " + StationViewHistory.TABLE_NAME+ " on " + Station.TABLE_NAME + "." + Station.C_ID + " = " + StationViewHistory.TABLE_NAME + "." + StationViewHistory.C_ID + " order by " + StationViewHistory.TABLE_NAME + "." + StationViewHistory.C_LAST_VIEWED + " desc")
    List<Station> loadStationsHistory();
}
