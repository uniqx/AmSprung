// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import at.h4x.amsprung.room.model.LineRoute;

@Dao
public interface LineRouteDao {
    @Query("select * from " + LineRoute.TABLE_NAME + " where " + LineRoute.C_LINE_ID + " = :lineId and " + LineRoute.C_STOP_ID + " = :stopId")
    LineRoute byIds(int lineId, int stopId);

    @Query("select * from " + LineRoute.TABLE_NAME + " where " + LineRoute.C_LINE_ID + " = :lineId")
    List<LineRoute> byLineId(int lineId);

    @Query("select * from " + LineRoute.TABLE_NAME + " where " + LineRoute.C_STOP_ID + " = :stopId")
    List<LineRoute> byStopId(int stopId);

    @Query("select exists (select 1 from " + LineRoute.TABLE_NAME + " where " + LineRoute.C_STOP_ID + " in (select " + Stop.C_ID + " from " + Stop.TABLE_NAME + " where " + Stop.C_STATION_ID + " = :stationId))")
    Boolean stationHasLines(int stationId);

    @Insert
    void insert(LineRoute... lineRoutes);

    @Update
    void update(LineRoute... lineRoutes);

    @Delete
    void delete(LineRoute line);
}