// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import lombok.Getter;
import lombok.Setter;

/**
 * imported from: wienerlinien_ogd_haltepunkte.csv
 * id = StopID
 * stationId = DIVA
 * communieId = MunicipalityID
 */
@Entity(tableName = Stop.TABLE_NAME)
public class Stop {

    public static final String TABLE_NAME = "user";
    public static final String C_ID = "id";
    public static final String C_STATION_ID = "station_id";
    public static final String C_NAME = "name";
    public static final String C_COMMUNE_NAME = "commune_name";
    public static final String C_COMMUNE_ID = "commune_id";
    public static final String C_LNG = "lng";
    public static final String C_LAT = "lat";

    @PrimaryKey
    @Getter @Setter public int id;

    @ColumnInfo(name = C_STATION_ID)
    @Getter @Setter Integer stationId;

    @ColumnInfo(name = C_NAME)
    @Getter @Setter String name;

    @ColumnInfo(name = C_COMMUNE_NAME)
    @Getter @Setter String communeName;

    @ColumnInfo(name = C_COMMUNE_ID)
    @Getter @Setter Integer communeId;

    @ColumnInfo(name = C_LNG)
    @Getter @Setter Double lng;

    @ColumnInfo(name = C_LAT)
    @Getter @Setter Double lat;
}
