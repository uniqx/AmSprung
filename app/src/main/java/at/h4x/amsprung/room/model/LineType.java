// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.room.model;

import androidx.room.TypeConverter;

import lombok.Getter;

public enum LineType {
    METRO(1, "ptMetro"),
    TRAIN_S(2, "ptTrainS"),
    TRAM(3, "ptTram"),
    TRAM_WLB(4, "ptTramWLB"),
    TRAM_VRT(5, "ptTramVRT"),
    BUS_CITY(6, "ptBusCity"),
    BUS_NIGHT(7, "ptBusNight"),
    OTHER(99, "other");

    @Getter
    private final int code;

    @Getter
    private final String name;

    LineType(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public static LineType fromCode(int typeCode) {
        for (LineType lineType : LineType.values()) {
            if(lineType.code == typeCode) {
                return lineType;
            }
        }
        throw new IllegalArgumentException("'" + typeCode +
                "' is not a valid code for a line type.");
    }

    public static LineType fromName(String name) {
        for (LineType lineType : LineType.values()) {
            if (lineType.name.equals(name)) {
                return lineType;
            }
        }
        throw new IllegalArgumentException("'" + name +
                "' is not a valid name for a line type.");
    }

    public static class Converter {
        @TypeConverter
        public static LineType toType(int type) {
            return LineType.fromCode(type);
        }
        @TypeConverter
        public static int toInteger(at.h4x.amsprung.room.model.LineType type) {
            return type.getCode();
        }
    }
}
