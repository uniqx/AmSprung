// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

public class NotificationHelper {

    public static final String FOREGROUND_SERVICE_CHANNEL_ID =
            "com.h4x.amsprung.channel.foreground_service";

    public static void registerChannels(Context context){
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationManager notificationManager = null;
            notificationManager = context.getSystemService(NotificationManager.class);
            NotificationChannel channel = null;
            channel = new NotificationChannel(FOREGROUND_SERVICE_CHANNEL_ID,
                    context.getString(R.string.notification_channel_foreground_notification_name),
                    NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(
                    context.getString(R.string.notification_channel_foreground_notification_description));
            notificationManager.createNotificationChannel(channel);
        }
    }
}
