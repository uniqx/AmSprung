// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.wienerlinienapi;

import android.os.AsyncTask;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import at.h4x.amsprung.R;
import at.h4x.amsprung.room.AppDatabase;
import at.h4x.amsprung.room.model.Line;
import at.h4x.amsprung.room.model.LineType;

/**
 * Created by uniq on 22.01.17.
 */
public class LineBadgeHelper {

    private static final int DEFAULT_BADGE_LINES = 2;
    private static final int DEFAULT_BADGES_PER_LINE = 2;

    public static final void lineBadgesForStation(final int stationId, final LinearLayout linearLayout) {
        lineBadgesForStation(stationId, linearLayout, DEFAULT_BADGE_LINES, DEFAULT_BADGES_PER_LINE);
    }

    public static final void lineBadgesForStation(final int stationId, final LinearLayout linearLayout, final int badgeLines, final int badgesPerLine) {
        new AsyncTask<Void, Void, List<Line>>() {
            @Override
            protected List<Line> doInBackground(Void... voids) {
                AppDatabase db = AppDatabase.via(linearLayout.getContext());

                List<Line> stops = db.lineDao().getLinesForStationId(stationId);
                Collections.sort(stops, lineComparator);

                return stops;
            }

            @Override
            protected void onPostExecute(List<Line> lines) {

                if (linearLayout.getChildCount() == 0  || !(linearLayout.getChildAt(0) instanceof LinearLayout)) {
                    linearLayout.removeAllViews();
                    linearLayout.setOrientation(LinearLayout.VERTICAL);
                    for (int i = 0; i < badgeLines; i++) {
                        LinearLayout l = new LinearLayout(linearLayout.getContext());
                        l.setOrientation(LinearLayout.HORIZONTAL);
                        linearLayout.addView(l);
                    }
                } else {
                    for (int i = 0; i < badgeLines; i++) {
                        ((LinearLayout)linearLayout.getChildAt(i)).removeAllViews();
                    }
                }

                int lineBadgeLimit = badgeLines * badgesPerLine;
                boolean putMoreIndicator = lines.size() > lineBadgeLimit;

                for (int i = 0; i < Math.min(lineBadgeLimit, lines.size()); i++) {
                    Line line = lines.get(i);
                    LinearLayout ll = (LinearLayout) linearLayout.getChildAt(i / badgesPerLine);
                    if (putMoreIndicator && i == lineBadgeLimit - 1) {
                        putBadge("...", LineType.BUS_CITY, ll);
                    } else {
                        putBadge(line.getName(), line.getType(), ll);
                    }
                }

                //linearLayout.setAlpha(0F);
                //linearLayout.animate().setDuration(600L).alpha(1F).start();
            }
        }.execute();
    }

    public static final void putBadge(String lineName, LineType lineType, LinearLayout linearLayout) {

        TextView textView = new TextView(linearLayout.getContext());

        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
        textView.setText(lineName);

        final float scale = linearLayout.getResources().getDisplayMetrics().density;
        int side_in_dp = 40;
        int side_in_px = (int) (side_in_dp * scale * 0.5f);
        textView.setGravity(Gravity.CENTER);
        textView.setHeight(side_in_px);
        textView.setWidth(side_in_px);

        int padding_in_dp = 3;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(padding_in_px, 0, 0, padding_in_px);
        textView.setLayoutParams(lp);

        LineBadgeHelper.format(textView, lineName, lineType);

        if (Build.VERSION.SDK_INT >= 21) {
            textView.setElevation(linearLayout.getElevation() + 1);
        }

        linearLayout.addView(textView);
    }

    public static final void format(TextView textView, String lineName, LineType lineType){

        if (lineName != null && lineName.length() >= 3) {
            textView.setTextScaleX(0.85f);
        } else {
            textView.setTextScaleX(1.0f);
        }

        switch (lineType){
            case METRO:
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.white));

                switch (lineName){
                    case "U1":
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.linetype_u1));
                        break;
                    case "U2":
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.linetype_u2));
                        break;
                    case "U3":
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.linetype_u3));
                        break;
                    case "U4":
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.linetype_u4));
                        break;
                    case "U5":
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.linetype_u5));
                        break;
                    case "U6":
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.linetype_u6));
                        break;
                    default:
                        textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.superlightgray));
                        textView.setTextColor(textView.getContext().getResources().getColor(R.color.black));
                }
                break;
            case TRAM:
            case TRAM_VRT:
            case TRAM_WLB:
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.white));
                textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.black));
                break;
            default:
                textView.setTextColor(textView.getContext().getResources().getColor(R.color.black));
                textView.setBackgroundColor(textView.getContext().getResources().getColor(R.color.superlightgray));
        }
    }

    private static final Comparator<Line> lineComparator = new Comparator<Line>() {
        @Override
        public int compare(Line l1, Line l2) {
            int level1 = lineTypeComparator.compare(l1, l2);
            if (level1 == 0) {
                return l1.getName().compareTo(l2.getName());
            } else {
                return level1;
            }
        }
    };

    private static final Comparator<Line> lineTypeComparator = new Comparator<Line>() {
        @Override
        public int compare(Line l1, Line l2) {
            return l1.getType().getCode() - l2.getType().getCode();
        }
    };

}
