// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.wienerlinienapi;

import android.os.AsyncTask;

/**
 * Created by uniq on 18.09.15.
 */
public abstract class WienerLinienApiAsyncTask<Param, TmpResult, Result> extends AsyncTask<Param, TmpResult, Result>{

    public static final String API_BASE_URL = "https://www.wienerlinien.at/ogd_realtime/monitor/";

}
