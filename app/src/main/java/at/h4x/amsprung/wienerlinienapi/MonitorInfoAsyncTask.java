// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.wienerlinienapi;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import at.h4x.amsprung.util.MinLog;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by uniq on 18.09.15.
 */
public class MonitorInfoAsyncTask extends WienerLinienApiAsyncTask<Integer, Void, List<MonitorInfoAsyncTask.MonitorInfo>> {

    @Override
    protected List<MonitorInfo> doInBackground(Integer... ints) {

        Gson gson = new Gson();
        List<MonitorInfo> monitorInfos = new ArrayList<>();

        for(int rbl : ints) {

            String url = WienerLinienApiAsyncTask.API_BASE_URL + "?rbl=" + rbl
                    + "&activateTrafficInfo=stoerungkurz&activateTrafficInfo=stoerunglang&activateTrafficInfo=aufzugsinfo";

            String response = HttpHelper.get(url);

            MinLog.v("%s -> %s", url, response);

            monitorInfos.add(gson.fromJson(response, MonitorInfo.class));
        }

        return monitorInfos;
    }


    @ToString
    public static class MonitorInfo {
        @Getter @Setter Data data;
        //@Getter @Setter Message message;
    }

    public static class Data {
        @Getter @Setter List<Monitor> monitors;
    }

    public static class Monitor {
        //@Getter @Setter LocationStop locationStop;
        @Getter @Setter List<Line> lines;
    }

    public static class Line{
        @Getter @Setter String name;
        @Getter @Setter String towards;
        @Getter @Setter Departures departures;
        @Getter @Setter String type;
    }

    public static class Departures {
        @Getter @Setter List<Departure> departure;
    }

    public static class Departure{
        @Getter @Setter DepartureTime departureTime;
    }

    public static class DepartureTime{
        @Getter @Setter int countdown;
    }

    public static class LocationStop {
        @Getter @Setter String type;
        @Getter @Setter LocationStopProperties properties;
        @Getter @Setter Geometry geometry;
    }

    public static class LocationStopProperties {
        @Getter @Setter String name;
        @Getter @Setter String title;
        @Getter @Setter String coordName;
        @Getter @Setter String type;
        @Getter @Setter Attributes attributes;
    }

    public static class Attributes{
        @Getter @Setter Integer rbl;
    }

    public static class Geometry{
        @Getter @Setter String type;
        @Getter @Setter List<Double> coordinates;
    }

     public static class Message {
        @Getter @Setter String value;
        @Getter @Setter Integer messageCode;
        @Getter @Setter String serverTime;
    }

}
