// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import at.h4x.amsprung.room.AppDatabase;
import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.util.MinLog;

public class SearchResultAdapter extends StationsAdapter {

    private final AppDatabase db;

    public SearchResultAdapter(Context applicationContext, ClickListener listener) {
        super(listener);
        db = AppDatabase.via(applicationContext);
    }

    public void updateQuery(final String query) {
        MinLog.v("(trace)");
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                if (query == null) {
                    MinLog.d("station search: 0 found (query null)");
                    updateStationsAdapter(new ArrayList<Station>());
                    return null;
                }

                String[] queryTokens = query.split(" ");

                if (queryTokens.length < 1){
                    MinLog.d("station search: 0 found (no query tokens)");
                    updateStationsAdapter(new ArrayList<Station>());
                    return null;
                }

                if (queryTokens[0].length() >= 3) {
                    if (queryTokens.length == 1) {
                        // search query consists just of 1 token so just search for that one
                        // in database and return
                        List<Station> stations = db.stationDao().findByName("%" + queryTokens[0] + "%");
                        MinLog.d("station search: %d found (single token)", stations.size());
                        updateStationsAdapter(stations);
                    } else {
                        // search query consists for a couple of tokens so search database for the
                        // first one. Filter the initial search result by subsequent tokens
                        List<Station> stations = new ArrayList<>();
                        for (Station station : db.stationDao().findByName("%" + queryTokens[0] + "%")){
                            boolean okay = true;
                            for (int i=1; i<queryTokens.length; i++) {
                                if(!station.getName().toLowerCase().contains(queryTokens[i].toLowerCase())) {
                                    okay = false;
                                }
                            }
                            if (okay) {
                                stations.add(station);
                            }
                        }
                        MinLog.d("station search: %d found (multi token)", stations.size());
                        updateStationsAdapter(stations);
                    }
                } else {
                    MinLog.d("station search: 0 found (token less than 3 chars)");
                    updateStationsAdapter(new ArrayList<Station>());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                MinLog.v("(trace)");
                notifyDataSetChanged();
                super.onPostExecute(aVoid);
            }
        }.execute();
    }
}
