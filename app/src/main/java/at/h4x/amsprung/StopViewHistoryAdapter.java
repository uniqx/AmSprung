// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.content.Context;
import android.os.AsyncTask;

import at.h4x.amsprung.room.AppDatabase;

public class StopViewHistoryAdapter extends StationsAdapter {

    private final AppDatabase db;

    public StopViewHistoryAdapter(Context context, ClickListener listener) {
        super(listener);
        db = AppDatabase.via(context);
    }

    public void update() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                updateStationsAdapter(db.stationViewHistoryDao().loadStationsHistory());
                return null;
            }
        }.execute();
    }
}
