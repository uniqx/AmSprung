// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A fragment representing a list of Items.
 */
public class StopViewHistoryFragment extends Fragment {

    private StopViewHistoryAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public StopViewHistoryFragment() {
    }

    public static StopViewHistoryFragment newInstance() {
        StopViewHistoryFragment fragment = new StopViewHistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_stopviewhistory_list, container, false);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new StopViewHistoryAdapter(getContext(), new StationsAdapter.ClickListener() {
            @Override
            public void onClicked(int stopId) {
                Intent i = new Intent(getActivity(), StationDetailsActivity.class);
                i.putExtra(StationDetailsActivity.EXTRA_STATION_ID, stopId);
                startActivity(i);
            }
        });
        recyclerView.setAdapter(adapter);

        return recyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.update();
    }

}
