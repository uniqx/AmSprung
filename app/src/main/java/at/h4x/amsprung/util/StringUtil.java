// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by uniq on 16.02.16.
 */
public class StringUtil {

    public static String joinString(String delimiter, String... elements){
        return joinString(delimiter, Arrays.asList(elements));
    }

    public static String joinString(String delimiter, List<String> elements){
        StringBuilder sb = new StringBuilder();
        boolean firstIteration = true;
        for (String s : elements){
            if (firstIteration){
                firstIteration = false;
            } else {
                sb.append(delimiter);
            }
            sb.append(s);
        }
        return sb.toString();
    }

}
