// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class AndroidUnitsUtil {

    public static float convertDpToPixel(Context context, float dp){
        int densityDpi = context.getResources().getDisplayMetrics().densityDpi;
        return dp * ((float) densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float convertPixelsToDp(Context context, float px){
        int densityDpi = context.getResources().getDisplayMetrics().densityDpi;
        return px / ((float) densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }


}
