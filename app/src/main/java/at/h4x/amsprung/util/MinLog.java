// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.util;

import android.util.Log;

import java.util.IllegalFormatException;
import java.util.Locale;

public class MinLog {

    private static String tag = "default";
    private static boolean enabled = true;

    public static void on(String tag) {
        MinLog.enabled = true;
        MinLog.tag = tag;
    }

    public static void v(Throwable throwable) {
        if (enabled) {
            Log.v(tag, formatMessage(""), throwable);
        }
    }

    public static void v(String formatString, Object... args) {
        if (enabled) {
            Log.v(tag, formatMessage(formatString, args));
        }
    }

    public static void v(Throwable throwable, String formatString, Object... args) {
        if (enabled) {
            Log.v(tag, formatMessage(formatString, args), throwable);
        }
    }

    public static void d(Throwable throwable) {
        if (enabled) {
            Log.d(tag, formatMessage(""), throwable);
        }
    }

    public static void d(String formatString, Object... args) {
        if (enabled) {
            Log.d(tag, formatMessage(formatString, args));
        }
    }

    public static void d(Throwable throwable, String formatString, Object... args) {
        if (enabled) {
            Log.d(tag, formatMessage(formatString, args), throwable);
        }
    }

    public static void i(Throwable throwable) {
        if (enabled) {
            Log.i(tag, formatMessage(""), throwable);
        }
    }

    public static void i(String formatString, Object... args) {
        if (enabled) {
            Log.i(tag, formatMessage(formatString, args));
        }
    }

    public static void i(Throwable throwable, String formatString, Object... args) {
        if (enabled) {
            Log.i(tag, formatMessage(formatString, args), throwable);
        }
    }

    public static void w(Throwable throwable) {
        if (enabled) {
            Log.w(tag, formatMessage(""), throwable);
        }
    }

    public static void w(String formatString, Object... args) {
        if (enabled) {
            Log.w(tag, formatMessage(formatString, args));
        }
    }

    public static void w(Throwable throwable, String formatString, Object... args) {
        if (enabled) {
            Log.w(tag, formatMessage(formatString, args), throwable);
        }
    }

    public static void e(Throwable throwable) {
        if (enabled) {
            Log.e(tag, formatMessage(""), throwable);
        }
    }

    public static void e(String formatString, Object... args) {
        if (enabled) {
            Log.e(tag, formatMessage(formatString, args));
        }
    }

    public static void e(Throwable throwable, String formatString, Object... args) {
        if (enabled) {
            Log.e(tag, formatMessage(formatString, args), throwable);
        }
    }

    public static void wtf(Throwable throwable) {
        if (enabled) {
            Log.wtf(tag, formatMessage(""), throwable);
        }
    }

    public static void wtf(String formatString, Object... args) {
        if (enabled) {
            Log.wtf(tag, formatMessage(formatString, args));
        }
    }

    public static void wtf(Throwable throwable, String formatString, Object... args) {
        if (enabled) {
            Log.wtf(tag, formatMessage(formatString, args), throwable);
        }
    }

    private static String formatMessage(String format, Object... args) {

        StringBuilder sb = new StringBuilder();

        // log current thread
        sb.append('[');
        sb.append(Thread.currentThread().getId());
        sb.append("] ");

        // fetch method which invoked us from stack trace and log it
        StackTraceElement[] trace = new Throwable().fillInStackTrace().getStackTrace();
        String caller = "<unknown>";
        for (int i = 2; i < trace.length; i++) {
            Class<?> clazz = trace[i].getClass();
            if (!clazz.equals(MinLog.class)) {
                String callingClass = trace[i].getClassName();
                callingClass = callingClass.substring(callingClass.lastIndexOf('.') + 1);
                callingClass = callingClass.substring(callingClass.lastIndexOf('$') + 1);

                caller = callingClass + "." + trace[i].getMethodName();
                break;
            }
        }
        sb.append(caller);
        sb.append(": ");

        // log arguments
        try {
            sb.append((args == null) ? format : String.format(Locale.US, format, args));
        } catch (IllegalFormatException e) {
            sb.append("FORMAT STRING BROKEN \"");
            sb.append(format);
            sb.append("\" % (");
            boolean first = true;
            if (args == null) {
                sb.append("null");
            } else {
                for (Object o : args) {
                    if (first) {
                        first = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(o == null ? "null" : o.getClass().getSimpleName());
                }
            }
            sb.append(")");
        }

        return sb.toString();
    }
}
