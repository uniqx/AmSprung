// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung.util;

import android.content.Context;
import okhttp3.OkHttpClient;

/**
 * Created by uniq on 26.04.17.
 */

public class OkHttpClientProvider {

    private static OkHttpClient okHttpClient = null;

    public static OkHttpClient get(Context context){
        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder().build();
        }
        return okHttpClient;
    }

}
