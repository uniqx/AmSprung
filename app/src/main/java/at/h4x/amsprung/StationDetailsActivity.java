// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import at.h4x.amsprung.room.AppDatabase;
import at.h4x.amsprung.room.model.FavouriteStation;
import at.h4x.amsprung.room.model.Station;
import at.h4x.amsprung.room.model.StationViewHistoryDao;
import at.h4x.amsprung.room.model.StationViewHistory;
import at.h4x.amsprung.util.MinLog;
import at.h4x.amsprung.util.OkHttpClientProvider;
import at.h4x.amsprung.wienerlinienapi.LineBadgeHelper;
import at.h4x.amsprung.wienerlinienapi.MonitorInfoAsyncTask;
import okhttp3.Request;
import okhttp3.Response;

public class StationDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_STATION_ID = "extra_station_id";
    private static final long IGNORE_REFRESH_INTERVAL = 10 * 1000;

    private HandlerThread backgroundThread;
    private Handler backgroundHandler;
    private final Handler mainHandler = new Handler(Looper.getMainLooper());

    private RecyclerView recycler;
    // TODO: probably can be eliminated
    private DeparturesAdapter adapter;
    private MenuItem menuActionFavOn;
    private MenuItem menuActionFavOff;
    private int stationId = 0;
    private SwipeRefreshLayout swipeRefresh;
    private long lastLoad = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        backgroundThread = new HandlerThread(
                this.getClass().getName(),
                HandlerThread.NORM_PRIORITY);
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());

        setContentView(R.layout.activity_station_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // enable toolbar back-button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // hack to make setting title working :(
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        // make floating action button disappear for now
        fab.setVisibility(View.GONE);

        recycler = (RecyclerView) findViewById(R.id.recycler);
        // weird hack to get nested scrolling working
        // https://stackoverflow.com/questions/31000081
        recycler.setNestedScrollingEnabled(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(layoutManager);

        stationId = getIntent().getIntExtra(EXTRA_STATION_ID, -1);

        swipeRefresh = findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (lastLoad + IGNORE_REFRESH_INTERVAL > System.currentTimeMillis()) {
                    swipeRefresh.setRefreshing(false);
                    MinLog.d("ignore refreshing, don't allow spamming the server");
                } else {
                    loadStation(stationId);
                }
            }
        });

        recordStationViewHistory(stationId);

        loadStation(stationId);
        swipeRefresh.setRefreshing(true);
        displayLineBadges();
    }

    private void recordStationViewHistory(final int stationId) {
        WeakReference<StationDetailsActivity> activity = new WeakReference<>(this);
        backgroundHandler.post(() -> {
            StationViewHistoryDao stationViewHistoryDao = AppDatabase.via(activity.get()).stationViewHistoryDao();
            StationViewHistory viewHist = stationViewHistoryDao.byId(stationId);
            if (viewHist == null) {
                viewHist = new StationViewHistory();
                viewHist.setId(stationId);
                viewHist.setViewCount(1);
                viewHist.setLastViewed(System.currentTimeMillis());
                stationViewHistoryDao.insert(viewHist);
            } else {
                viewHist.setViewCount(viewHist.getViewCount() + 1);
                viewHist.setLastViewed(System.currentTimeMillis());
                stationViewHistoryDao.update(viewHist);
                MinLog.d("update view count for station[%d] to %d (%d)", stationId, viewHist.getViewCount(), viewHist.getLastViewed());
            }
        });
    }

    private void displayLineBadges() {
        LineBadgeHelper.lineBadgesForStation(stationId, (LinearLayout) findViewById(R.id.line_badges), 6, 4);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_station_details, menu);
        // make fav icons disappear
        menuActionFavOn = menu.findItem(R.id.action_fav_on);
        menuActionFavOn.setVisible(false);
        menuActionFavOff = menu.findItem(R.id.action_fav_off);
        menuActionFavOff.setVisible(false);

        updateFavMenuOptions();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_fav_on:
                actionUnfavourite();
                return true;
            case R.id.action_fav_off:
                actionFavourite();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void actionFavourite() {
        WeakReference<StationDetailsActivity> activity = new WeakReference<>(this);
        backgroundHandler.post(() -> {
            AppDatabase db = AppDatabase.via(activity.get());
            FavouriteStation favStation = new FavouriteStation();
            favStation.setId(activity.get().stationId);
            db.favouriteStationDao().insert(favStation);
            mainHandler.post(() -> activity.get().updateFavMenuOptions());
        });
    }

    private void actionUnfavourite() {
        WeakReference<StationDetailsActivity> activity = new WeakReference<>(this);
        backgroundHandler.post(() -> {
            AppDatabase db = AppDatabase.via(activity.get());
            db.favouriteStationDao().deleteById(activity.get().stationId);
            mainHandler.post(() -> activity.get().updateFavMenuOptions());
        });
    }

    private void loadStation(final int stationId) {
        WeakReference<StationDetailsActivity> activity = new WeakReference<>(this);
        backgroundHandler.post(() -> {
            AppDatabase db = AppDatabase.via(StationDetailsActivity.this);
            final Station station = db.stationDao().byId(stationId);
            mainHandler.post(() -> {
                activity.get().displayStation(station);
                activity.get().loadAndDisplayMonitorsForStation(station);
            });
        });
    }

    void loadAndDisplayMonitorsForStation(Station station) {
        WeakReference<StationDetailsActivity> activity = new WeakReference<>(this);
        backgroundHandler.post(() -> {
            List<MonitorInfoAsyncTask.MonitorInfo> monitorInfos = new ArrayList<MonitorInfoAsyncTask.MonitorInfo>();
            if (station != null && station.getId() != null && station.getId() != 0) {
                final String url = "https://www.wienerlinien.at/ogd_realtime/monitor/?diva="
                        + station.getId()
                        + "&activateTrafficInfo=stoerungkurz&activateTrafficInfo=stoerunglang&activateTrafficInfo=aufzugsinfo";
                MinLog.v("request url: %s", url);

                Request request = new Request.Builder().url(url).build();
                try {
                    Response response = OkHttpClientProvider.get(activity.get()).newCall(request).execute();
                    if (response.code() == 200) {
                        String responseString = response.body().string();
                        MinLog.v("response: %s", responseString);
                        MonitorInfoAsyncTask.MonitorInfo i = new Gson().fromJson(responseString, MonitorInfoAsyncTask.MonitorInfo.class);
                        if (i != null) {
                            monitorInfos.add(i);
                        }
                    } else {
                        MinLog.e("http request failed (url: '" + url + "', respnse code: " + response.code() + ")");
                    }
                } catch (JsonSyntaxException e) {
                    MinLog.e(e, "http request failed (url: '" + url + "')");
                } catch (IOException e) {
                    MinLog.e(e, "http request failed (url: '" + url + "')");
                }
            }

            mainHandler.post(() -> {
                if (activity.get().adapter == null) {
                    activity.get().adapter = new DeparturesAdapter(monitorInfos);
                    activity.get().recycler.setAdapter(activity.get().adapter);
                } else {
                    activity.get().adapter = new DeparturesAdapter(monitorInfos);
                    activity.get().recycler.swapAdapter(activity.get().adapter, true);
                }
                activity.get().swipeRefresh.setRefreshing(false);
            });
        });
    }

    void displayStation(final Station station) {
        if (station != null && station.getName() != null) {

            getSupportActionBar().setTitle(station.getName());
            // hack to make set title working :(
            getSupportActionBar().setDisplayShowTitleEnabled(true);

            ((TextView) findViewById(R.id.stationCommune)).setText(station.getCommuneName());
        }
    }

    protected void updateFavMenuOptions() {
        WeakReference<StationDetailsActivity> activity = new WeakReference<>(this);
        backgroundHandler.post(() -> {
            AppDatabase db = AppDatabase.via(activity.get());
            FavouriteStation favStation = db.favouriteStationDao().byId(activity.get().stationId);
            boolean isFavouriteStation = favStation == null;
            mainHandler.post(() -> {
                if (activity.get().menuActionFavOff != null && activity.get().menuActionFavOn != null){
                    activity.get().menuActionFavOn.setVisible(!isFavouriteStation);
                    activity.get().menuActionFavOff.setVisible(isFavouriteStation);
                }
            });
        });
    }
}
