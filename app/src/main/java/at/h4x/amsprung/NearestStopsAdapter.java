// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import at.h4x.amsprung.room.AppDatabase;
import at.h4x.amsprung.room.model.Stop;
import at.h4x.amsprung.wienerlinienapi.LineBadgeHelper;

/**
 * Created by uniq on 20.01.17.
 */
public class NearestStopsAdapter extends RecyclerView.Adapter<NearestStopsAdapter.NearestStopsAdapterViewHolder> {
    private final Context context;

    private Map<Long, Stop> nearestStops = new TreeMap<>();


    public NearestStopsAdapter(Context context) {
        this.context = context.getApplicationContext();
    }


    @Override
    public NearestStopsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stop_distance, parent, false);
        // set the view's size, margins, paddings and layout parameters
        NearestStopsAdapterViewHolder vh = new NearestStopsAdapterViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(NearestStopsAdapterViewHolder holder, int position) {

        long dist = (Long) nearestStops.keySet().toArray()[position];
        //final StopModel stop = nearestStops.get(dist);
        final Stop stop = nearestStops.get(dist);

        holder.stopName.setText(stop.getName());
        holder.communeName.setText(stop.getCommuneName());
        if (dist < 495){
            holder.distance.setText(Math.round(dist*.1f)*10 + "m");
        } else {
            holder.distance.setText(String.format("%.1fkm", dist * .001));
        }

        LineBadgeHelper.lineBadgesForStation(stop.getId(), holder.lines);

        holder.itemView.setAlpha(0f);
        holder.itemView.animate().alpha(1f).setDuration(600).start();

        holder.clicky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "clicky...", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(v.getContext(), StationDetailsActivity.class);
                i.putExtra(StationDetailsActivity.EXTRA_STATION_ID, stop.getId());
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nearestStops.size();
    }

    public void setLocation(final double searchLng, final double searchLat) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                AppDatabase db = AppDatabase.via(context);
                // TODO: bounding box?!
                List<Stop> allStops = db.stopDao().getAll();

                float[] dstResult = new float[3];
                Map<Long, Stop> nearest = new TreeMap<>();
                long biggestIn = 0;
                for (Stop stop : allStops) {
                    Location.distanceBetween(searchLat, searchLng, stop.getLat(), stop.getLng(), dstResult);
                    long dist = (long) dstResult[0];
                    if (nearest.size() < 20) {
                        nearest.put(dist, stop);
                        if (dist > biggestIn) {
                            biggestIn = dist;
                        }
                    } else {
                        if (dist < biggestIn) {
                            nearest.remove(biggestIn);
                            nearest.put(dist, stop);
                            biggestIn = 0;
                            for (long l : nearest.keySet()) {
                                biggestIn = l > biggestIn ? l : biggestIn;
                            }
                        }
                    }
                }
                NearestStopsAdapter.this.nearestStops = nearest;

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                notifyDataSetChanged();
            }
        }.execute();
    }

    public static class NearestStopsAdapterViewHolder extends RecyclerView.ViewHolder {

        View clicky;
        TextView stopName;
        TextView communeName;
        TextView distance;
        LinearLayout lines;

        public NearestStopsAdapterViewHolder(View itemView) {
            super(itemView);
            clicky = itemView.findViewById(R.id.item);
            stopName = (TextView) itemView.findViewById(R.id.stationName);
            communeName = (TextView) itemView.findViewById(R.id.stationCommune);
            distance = (TextView) itemView.findViewById(R.id.distance);
            lines = (LinearLayout) itemView.findViewById(R.id.lines);
        }
    }
}
