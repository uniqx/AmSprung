// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import at.h4x.amsprung.util.AndroidUnitsUtil;
import at.h4x.amsprung.util.MinLog;

public class HomeActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabs;
    private MenuItem searchMenuItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.round_more_vert_white_36));
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, SearchActivity.class));
            }
        });

        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new HomePager(getSupportFragmentManager()));

        CollapsingToolbarLayout toolbarLayout = findViewById(R.id.toolbar_layout);
        AppBarLayout appBarLayout = findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new OffsetObserver(fab));

        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.getTabAt(0).setCustomView(getLayoutInflater().inflate(R.layout.tab_header_favs, null));
        tabs.getTabAt(1).setCustomView(getLayoutInflater().inflate(R.layout.tab_header_history, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);

        searchMenuItem = menu.findItem(R.id.action_search);

        if (BuildConfig.DEBUG) {
            ((MenuItem) menu.findItem(R.id.action_debug)).setVisible(true);
        } else {
            ((MenuItem) menu.findItem(R.id.action_debug)).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_debug:
                startActivity(new Intent(this, DebugActivity.class));
                return true;
            case R.id.action_search:
                startActivity(new Intent(this, SearchActivity.class));
                return true;
            case R.id.action_settings:
                Toast.makeText(this, "TODO: implement settings", Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_about:
                //Snackbar.make(getWindow().getDecorView(), "TODO: implement about activity", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                String msg = getString(R.string.about_message);
                String versionInfo = BuildConfig.VERSION_NAME;
                if (!"release".equals(BuildConfig.BUILD_TYPE)) {
                    versionInfo += " (" + BuildConfig.BUILD_TYPE + ")";
                }
                msg = msg.replace("{versionInfo}", versionInfo);
                Spanned spannedMsg = Build.VERSION.SDK_INT >= 24 ? Html.fromHtml(msg, Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(msg);
                TextView tvAboutContent = new TextView(this);
                int dp25 = Math.round(25f*(getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
                int dp10 = Math.round(10f*(getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
                tvAboutContent.setPadding(dp25, dp10, dp25, dp10);
                tvAboutContent.setText(spannedMsg);
                tvAboutContent.setMovementMethod(LinkMovementMethod.getInstance());
                new AlertDialog.Builder(this)
                    .setTitle(R.string.about_title)
                    .setIcon(R.mipmap.ic_launcher)
                    .setView(tvAboutContent)
                    .setNegativeButton(R.string.about_close, new AlertDialog.OnClickListener(){
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    }).create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class HomePager extends FragmentStatePagerAdapter {

        public HomePager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return FavouriteStationFragment.newInstance();
                case 1:
                    return StopViewHistoryFragment.newInstance();
                default:
                    throw new IndexOutOfBoundsException(String.format("No fragment available for position: %d", position));
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    class OffsetObserver implements AppBarLayout.OnOffsetChangedListener{

        private final float targetOffset;
        private final FloatingActionButton fab;
        boolean targetOffsetExeeded = false;

        OffsetObserver(FloatingActionButton fab) {
            targetOffset = AndroidUnitsUtil.convertDpToPixel(HomeActivity.this, -30);
            this.fab = fab;
        }

        @Override
        public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
            if (!targetOffsetExeeded && verticalOffset < targetOffset) {
                MinLog.v("hide floating action button");
                targetOffsetExeeded = true;
                fab.hide();
                if (searchMenuItem != null) {
                    searchMenuItem.setVisible(true);
                }
            } else if (targetOffsetExeeded && verticalOffset >= targetOffset) {
                MinLog.v("show floating action button");
                targetOffsetExeeded = false;
                fab.show();
                if (searchMenuItem != null) {
                    searchMenuItem.setVisible(false);
                }
            }
        }

    }
}
