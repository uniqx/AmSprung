// SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
// SPDX-License-Identifier: GPL-3.0-or-later

package at.h4x.amsprung;

import android.os.AsyncTask;

import androidx.multidex.MultiDexApplication;

import java.lang.ref.WeakReference;

import at.h4x.amsprung.room.AppDatabase;
import at.h4x.amsprung.room.model.Stop;
import at.h4x.amsprung.util.MinLog;

/**
 * Created by uniq on 18.09.15.
 */
public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            MinLog.on("###");
        }

        // OkHttpClientProvider.initOrbot(this);
        NotificationHelper.registerChannels(this);

        // TODO: better implementation of makeshift first-run db import...
        new InitDBAsyncTask(this).execute();
    }

    private static class InitDBAsyncTask extends AsyncTask<Void, Void, Void> {

        private final WeakReference<App> context;

        InitDBAsyncTask(App context) {
            this.context = new WeakReference<>(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            AppDatabase db = AppDatabase.via(context.get());
            Stop stop = db.stopDao().byId(214550392); // = Seestadt Station
            if (stop == null || stop.getName() == null) {
                try {
                    UpdateAppDatabaseService.startActionUpdateDatabase(context.get());
                } catch (RuntimeException e) {
                    MinLog.w(e, "could not update database.");
                }
            }
            return null;
        }
    }
}
