# SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/uniq/bin/adt-bundle-linux-x86_64-20140702/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
