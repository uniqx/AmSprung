<!--
SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Am Sprung

public transport departure times in the city of Vienna

A small app for displaying upcoming departure times of Viennese public transportation stations. Data is retrieved via the public API of Wiener Linien.

"Am Sprung sein" is a Viennese colloquialism for departing.

<img alt="Am Sprung Icon" src="https://codeberg.org/uniqx/AmSprung/media/branch/main/app/src/main/ic_launcher-playstore.png" height="80"/>

<a href="https://f-droid.org/packages/at.h4x.amsprung">
  <img alt="Get it on F-Droid" src="https://codeberg.org/uniqx/AmSprung/media/branch/main/dev-assets/fdroid-badge.png" height="80" />
</a>

<a href="https://liberapay.com/uniqx/donate">
  <img alt="Get it on F-Droid" src="https://codeberg.org/uniqx/AmSprung/media/branch/main/dev-assets/liberapay-donate.png" height="80" />
</a>

## Screenshots

<a href="https://codeberg.org/uniqx/AmSprung/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot1.png">
  <img alt="Screenshot: Door status and calendar" src="https://codeberg.org/uniqx/AmSprung/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot1.png" width="150" />
</a>

<a href="https://codeberg.org/uniqx/AmSprung/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot2.png">
  <img alt="Screenshot: umbrella lighting controls" src="https://codeberg.org/uniqx/AmSprung/media/branch/main/metadata/en-US/images/phoneScreenshots/screenshot2.png" width="150" />
</a>

## License

This project is GPL-3.0-or-later licensed.

[![REUSE status](https://api.reuse.software/badge/codeberg.org/uniqx/AmSprung)](https://api.reuse.software/info/codeberg.org/uniqx/AmSprung)
