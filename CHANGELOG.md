<!--
SPDX-FileCopyrightText: 2021 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.5] - 2022-07-12
### added
- show keyboard when staring search activity

## [0.1.4] - 2022-05-19
### added
- norwegian translation

### Fixed
- no start-up crash on android 12
- corrected some typos

## [0.1.3] - 2022-03-26
### Added
- french translation

## [0.1.2] - 2022-02-26
### Added
- german translation
- app icon to store listing metadata

## [0.1.1] - 2021-11-18
### Added
- privacy infos in about dialog

### Fixed
- no more crashes on android Oreo and newer

## [0.1.0] - 2021-11-14
### Added
- inital release

[Unreleased]: https://codeberg.org/uniqx/AmSprung/src/branch/master
[0.1.5]: https://codeberg.org/uniqx/AmSprung/src/tag/v0.1.5
[0.1.4]: https://codeberg.org/uniqx/AmSprung/src/tag/v0.1.4
[0.1.3]: https://codeberg.org/uniqx/AmSprung/src/tag/v0.1.3
[0.1.2]: https://codeberg.org/uniqx/AmSprung/src/tag/v0.1.2
[0.1.1]: https://codeberg.org/uniqx/AmSprung/src/tag/v0.1.1
[0.1.0]: https://codeberg.org/uniqx/AmSprung/src/tag/v0.1.0
